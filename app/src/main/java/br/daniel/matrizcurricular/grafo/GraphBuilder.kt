package br.daniel.matrizcurricular.grafo

import br.daniel.matrizcurricular.model.CComputacao.*
import br.daniel.matrizcurricular.model.Disciplina

object GraphBuilder {

    private lateinit var graph: MutableList<Disciplina>

    fun getGraph(): MutableList<Disciplina> {
        return graph
    }

    fun buildGraphCComputacao() {

        val disciplina11 = Disciplina(MATEMATICA_DISCRETA)
        val disciplina12 = Disciplina(INTRODUCAO_COMPUTACAO)
        val disciplina13 = Disciplina(LOGICA_PROGRAMACAO)
        val disciplina14 = Disciplina(INFORMATICA_SOCIEDADE)
        val disciplina15 = Disciplina(CALCULO_I)

        val disciplina21 = Disciplina(ALGEBRA_LINEAR_GEOMETRIA_ANALITICA)
        val disciplina22 = Disciplina(SISTEMAS_LOGICOS_DIGITAIS)
        val disciplina23 = Disciplina(PROJETO_INTERFACE)
        val disciplina24 = Disciplina(CALCULO_II)
        val disciplina25 = Disciplina(PROG_ORIENTADA_OBJETO)

        val disciplina31 = Disciplina(LOGICA_MATEMATICA)
        val disciplina32 = Disciplina(TECNICAS_PROGRAMACAO)
        val disciplina33 = Disciplina(ARQUITETURA_ORG_COMPUTADORES)
        val disciplina34 = Disciplina(ADMIN_EMPREENDEDORISMO_INOVACAO)
        val disciplina35 = Disciplina(ESTRUTURA_DE_DADOS)

        val disciplina41 = Disciplina(TECNOLOGIAS_INTERNET_I)
        val disciplina42 = Disciplina(TEORIA_GRAFOS)
        val disciplina43 = Disciplina(CALCULO_NUMERICO)
        val disciplina44 = Disciplina(PROBABILIDADE_ESTATISTICA)
        val disciplina45 = Disciplina(FUNDAMENTOS_BANCO_DADOS)

        val disciplina51 = Disciplina(PROD_TRABALHO_CIENTIFICO)
        val disciplina52 = Disciplina(GESTAO_TECNOLOGIA_INFORMACAO)
        val disciplina53 = Disciplina(PROJETO_ANALISE_ALGORITMOS)
        val disciplina54 = Disciplina(TECNICAS_IMPLEMENTACAO_SISTEMAS_BD)
        val disciplina55 = Disciplina(ENGENHARIA_REQUISITOS_TESTE_SOFTWARE)
        val disciplina56 = Disciplina(SISTEMAS_OPERACIONAIS)

        val disciplina61 = Disciplina(ANALISE_PROJETO_SISTEMAS_II)
        val disciplina62 = Disciplina(PESQUISA_OPERACIONAL)
        val disciplina63 = Disciplina(TEORIA_AUT_LINGUENS_FORMAIS)
        val disciplina64 = Disciplina(PARADIGMAS_LINGUAGEM_PROGRAMACAO)
        val disciplina65 = Disciplina(REDES_DE_COMPUTADORES)

        val disciplina71 = Disciplina(ENGENHARIA_SOFTWARE)
        val disciplina72 = Disciplina(INTELIGENCIA_ARTIFICIAL)
        val disciplina73 = Disciplina(COMPUTACAO_GRAFICA)
        val disciplina74 = Disciplina(COMPUTABILIDADE)
        val disciplina75 = Disciplina(REDES_DE_COMPUTADORES_II)
        val disciplina76 = Disciplina(TRABALHO_CONCLUSAO_CURSO_I)

        val disciplina81 = Disciplina(COMPILADORES_I)
        val disciplina82 = Disciplina(GERENCIA_PROJETOS)
        val disciplina83 = Disciplina(PROCESSAMENTO_IMAGENS)
        val disciplina84 = Disciplina(TRABALHO_CONCLUSAO_CURSO_II)
        val disciplina85 = Disciplina(SISTEMAS_DISTRIBUIDOS)

        disciplina11.apply {
            addRequisito(disciplina22)
            addRequisito(disciplina31)
        }
        disciplina13.apply {
            addRequisito(disciplina23)
            addRequisito(disciplina25)
            addRequisito(disciplina43)
        }
        disciplina15.apply {
            addRequisito(disciplina24)
        }

        disciplina21.apply {
            addRequisito(disciplina43)
            addRequisito(disciplina83)
        }
        disciplina22.apply {
            addRequisito(disciplina33)
            addPreRequisito(disciplina11)
        }
        disciplina23.apply {
            addRequisito(disciplina55)
            addPreRequisito(disciplina13)
        }
        disciplina24.apply {
            addRequisito(disciplina43)
            addPreRequisito(disciplina15)
        }
        disciplina25.apply {
            addRequisito(disciplina35)
            addRequisito(disciplina32)
            addRequisito(disciplina56)
            addPreRequisito(disciplina13)
        }

        disciplina31.apply {
            addRequisito(disciplina63)
            addRequisito(disciplina72)
            addPreRequisito(disciplina11)
        }
        disciplina32.apply {
            addRequisito(disciplina41)
            addRequisito(disciplina55)
            addRequisito(disciplina73)
            addPreRequisito(disciplina25)
        }
        disciplina33.apply {
            addRequisito(disciplina56)
            addPreRequisito(disciplina22)
        }
        disciplina34.apply {
            addRequisito(disciplina52)
        }
        disciplina35.apply {
            addRequisito(disciplina45)
            addRequisito(disciplina42)
            addRequisito(disciplina64)
            addRequisito(disciplina83)
            addPreRequisito(disciplina25)
        }

        disciplina41.apply {
            addPreRequisito(disciplina32)
        }
        disciplina42.apply {
            addRequisito(disciplina53)
            addRequisito(disciplina63)
            addPreRequisito(disciplina35)
        }
        disciplina43.apply {
            addRequisito(disciplina62)
            addRequisito(disciplina73)
            addPreRequisito(disciplina21)
            addPreRequisito(disciplina24)
            addPreRequisito(disciplina13)
        }
        disciplina44.apply {
            addRequisito(disciplina53)
        }
        disciplina45.apply {
            addRequisito(disciplina54)
            addPreRequisito(disciplina35)
        }

        disciplina51.apply {
            addRequisito(disciplina76)
        }
        disciplina52.apply {
            addPreRequisito(disciplina34)
        }
        disciplina53.apply {
            addRequisito(disciplina72)
            addPreRequisito(disciplina44)
            addPreRequisito(disciplina42)

        }
        disciplina54.apply {
            addRequisito(disciplina76)
            addPreRequisito(disciplina45)
        }
        disciplina55.apply {
            addRequisito(disciplina61)
            addRequisito(disciplina76)
            addPreRequisito(disciplina23)
            addPreRequisito(disciplina32)
        }
        disciplina56.apply {
            addRequisito(disciplina65)
            addPreRequisito(disciplina33)
            addPreRequisito(disciplina25)
        }

        disciplina61.apply {
            addRequisito(disciplina71)
            addRequisito(disciplina85)
            addRequisito(disciplina82)
            addPreRequisito(disciplina55)
        }
        disciplina62.apply {
            addPreRequisito(disciplina43)
        }
        disciplina63.apply {
            addRequisito(disciplina74)
            addRequisito(disciplina81)
            addPreRequisito(disciplina31)
            addPreRequisito(disciplina42)
        }
        disciplina64.apply {
            addRequisito(disciplina72)
            addPreRequisito(disciplina35)
        }
        disciplina65.apply {
            addRequisito(disciplina76)
            addRequisito(disciplina75)
            addRequisito(disciplina85)
            addPreRequisito(disciplina56)
        }

        disciplina71.apply {
            addPreRequisito(disciplina61)
        }
        disciplina72.apply {
            addPreRequisito(disciplina31)
            addPreRequisito(disciplina64)
            addPreRequisito(disciplina53)
        }
        disciplina73.apply {
            addPreRequisito(disciplina43)
            addPreRequisito(disciplina32)
        }
        disciplina74.apply {
            addPreRequisito(disciplina63)
        }
        disciplina75.apply {
            addPreRequisito(disciplina65)
        }
        disciplina76.apply {
            addRequisito(disciplina84)
            addPreRequisito(disciplina51)
            addPreRequisito(disciplina54)
            addPreRequisito(disciplina65)
            addPreRequisito(disciplina55)
        }

        disciplina81.apply {
            addPreRequisito(disciplina63)
        }
        disciplina82.apply {
            addPreRequisito(disciplina61)
        }
        disciplina83.apply {
            addPreRequisito(disciplina21)
            addPreRequisito(disciplina35)
        }
        disciplina84.apply {
            addPreRequisito(disciplina76)
        }
        disciplina85.apply {
            addPreRequisito(disciplina61)
            addPreRequisito(disciplina65)
        }

        graph =  ArrayList<Disciplina>().apply {
            add(disciplina11)
            add(disciplina12)
            add(disciplina13)
            add(disciplina14)
            add(disciplina15)
            add(disciplina21)
            add(disciplina22)
            add(disciplina23)
            add(disciplina24)
            add(disciplina25)
            add(disciplina31)
            add(disciplina32)
            add(disciplina33)
            add(disciplina34)
            add(disciplina35)
            add(disciplina41)
            add(disciplina42)
            add(disciplina43)
            add(disciplina44)
            add(disciplina45)
            add(disciplina51)
            add(disciplina52)
            add(disciplina53)
            add(disciplina54)
            add(disciplina55)
            add(disciplina56)
            add(disciplina61)
            add(disciplina62)
            add(disciplina63)
            add(disciplina64)
            add(disciplina65)
            add(disciplina71)
            add(disciplina72)
            add(disciplina73)
            add(disciplina74)
            add(disciplina75)
            add(disciplina76)
            add(disciplina81)
            add(disciplina82)
            add(disciplina83)
            add(disciplina84)
            add(disciplina85)
        }

    }

}