package br.daniel.matrizcurricular.grafo

import br.daniel.matrizcurricular.model.Disciplina

class SuggestionListBuilder {

    private lateinit var suggestionList: MutableList<MutableList<Disciplina>>
    private lateinit var graph: MutableList<Disciplina>

    var hasCompleteOnStart: Boolean = false

    fun setGraph(graph: MutableList<Disciplina>): SuggestionListBuilder {
        this.graph = graph
        return this
    }

    fun getSuggestionList(): MutableList<MutableList<Disciplina>> {
        return suggestionList
    }

    fun buildSuggestionListOf(limit: Int) {
        val suggestionList = ArrayList<MutableList<Disciplina>>()
        val disciplinasCompleted = getDisciplinasComplete()
        val candidates: MutableList<Disciplina> = ArrayList()

        hasCompleteOnStart = if (disciplinasCompleted.size > 0) {
            suggestionList.add(disciplinasCompleted.toMutableList())
            candidates.addAll(getNewCandidatas(suggestionList))
            true
        } else {
            candidates.addAll(getFontes())
            false
        }

        while (candidates.size != 0) {

            candidates.sort()

            suggestionList.add(candidates
                    .subList(0,
                            if (candidates.size <= limit) {
                                candidates.size
                            } else {
                                limit
                            }
                    ).toMutableList()
            )

            candidates.clear()

            markDisciplinasAsComplete(suggestionList)
            candidates.addAll(getNewCandidatas(suggestionList))

        }

        this.suggestionList = suggestionList

    }

    private fun getFontes(): MutableList<Disciplina> {
        val fontes = ArrayList<Disciplina>()
        graph.forEach { disciplina ->
            if (!disciplina.hasPreRequisito()) {
                if (!disciplina.isComplete) {
                    fontes.add(disciplina)
                }
            }
        }
        return fontes
    }

    private fun getDisciplinasComplete(): MutableList<Disciplina> {
        val disciplinasComplete = ArrayList<Disciplina>()
        graph.forEach { disciplina ->
            if (disciplina.isComplete) {
                disciplinasComplete.add(disciplina)
            }
        }
        return disciplinasComplete
    }

    private fun markDisciplinasAsComplete(suggestionList: MutableList<MutableList<Disciplina>>) {
       suggestionList.forEach { disciplinas ->
           disciplinas.forEach { disciplina ->
               disciplina.isComplete = true
           }
       }
    }

    private fun getNewCandidatas(suggestionList: MutableList<MutableList<Disciplina>>): ArrayList<Disciplina> {
        val newCandidatas = ArrayList<Disciplina>()
        newCandidatas.addAll(getFontes())
        suggestionList.forEach { disciplinas ->
            disciplinas.forEach { disciplina ->
                disciplina.requisito.forEach { requisito ->
                    if (requisito.isPreRequisitoComplete()
                            && !requisito.isComplete
                            && !newCandidatas.contains(requisito)) {
                        newCandidatas.add(requisito)
                    }
                }
            }
        }
        return newCandidatas
    }

}
