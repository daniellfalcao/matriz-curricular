package br.daniel.matrizcurricular.view.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import br.daniel.matrizcurricular.R
import br.daniel.matrizcurricular.grafo.GraphBuilder
import br.daniel.matrizcurricular.grafo.SuggestionListBuilder
import br.daniel.matrizcurricular.view.adapter.DisciplinasAdapter
import kotlinx.android.synthetic.main.activity_matriz_curricular.*
import kotlinx.android.synthetic.main.content_semestre.view.*

class SugestaoMatriculaActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_QTD_DISCIPLINAS = "EXTRA_QTD_DISCIPLINAS"
    }

    private val suggestionListBuilder by lazy { SuggestionListBuilder() }

    private var limit: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activiy_sugestao_matricula)

        limit = intent.getIntExtra(EXTRA_QTD_DISCIPLINAS, 0)

        setupToolbar()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        suggestionListBuilder.setGraph(GraphBuilder.getGraph()).buildSuggestionListOf(limit)

        suggestionListBuilder.getSuggestionList().forEachIndexed { index, disciplinas ->
            val view = layoutInflater.inflate(R.layout.content_semestre, container, false)
            with(view) {
                if (suggestionListBuilder.hasCompleteOnStart) {
                    if (index == 0) {
                        semestre.text = context.getString(R.string.disciplinas_cursadas)
                    } else {
                        semestre.text = getString(R.string.header_semestre, index)
                    }
                } else {
                    semestre.text = getString(R.string.header_semestre, index.plus(1))
                }
                disciplinasRecyclerView.isNestedScrollingEnabled = false
                disciplinasRecyclerView.adapter = DisciplinasAdapter().apply { initSuggestionList(disciplinas) }
            }
            container.addView(view)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupToolbar() {
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.title = getString(R.string.toolbar_title_sugestao_matricula)
        }
    }



}