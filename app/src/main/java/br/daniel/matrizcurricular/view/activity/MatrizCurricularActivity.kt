package br.daniel.matrizcurricular.view.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.daniel.matrizcurricular.R
import br.daniel.matrizcurricular.grafo.GraphBuilder
import br.daniel.matrizcurricular.view.activity.SugestaoMatriculaActivity.Companion.EXTRA_QTD_DISCIPLINAS
import br.daniel.matrizcurricular.view.adapter.DisciplinasAdapter
import kotlinx.android.synthetic.main.activity_matriz_curricular.*
import kotlinx.android.synthetic.main.content_semestre.view.*
import kotlinx.android.synthetic.main.dialog_escolher_qtd_disciplinas.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity

class MatrizCurricularActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_matriz_curricular)

        buttonSugestao.setOnClickListener { onSugestaoClicked() }
    }

    override fun onResume() {
        super.onResume()
        GraphBuilder.buildGraphCComputacao()
        container.removeAllViews()

        for (index in 1..8) {
            val view = layoutInflater.inflate(R.layout.content_semestre, container, false)
            with(view) {
                semestre.text = getString(R.string.header_semestre, index)
                disciplinasRecyclerView.isNestedScrollingEnabled = false
                disciplinasRecyclerView.adapter = DisciplinasAdapter().apply { initCurricularMatrix(GraphBuilder.getGraph(), index) }
            }
            container.addView(view)
        }
    }

    private fun onSugestaoClicked() {
        alert {
            val view = layoutInflater.inflate(R.layout.dialog_escolher_qtd_disciplinas, null, false)
            val seekBar = view.seekBar
            title = "Disciplinas por semestre"
            customView = view
            positiveButton("CONTINUAR") {startActivity<SugestaoMatriculaActivity>(EXTRA_QTD_DISCIPLINAS to seekBar.progress)}
            negativeButton("CANCELAR") {}
        }.show()
    }
}
