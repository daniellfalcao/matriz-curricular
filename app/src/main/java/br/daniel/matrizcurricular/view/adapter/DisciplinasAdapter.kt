package br.daniel.matrizcurricular.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import br.daniel.matrizcurricular.model.Disciplina
import br.daniel.matrizcurricular.view.holder.DisciplinaViewHolder

class DisciplinasAdapter : RecyclerView.Adapter<DisciplinaViewHolder>() {

    private lateinit var disciplinas: List<Disciplina>
    private var isClickable: Boolean = false

    private val onCLick: (position: Int) -> Unit = {
        notifyItemChanged(it)
    }

    fun initSuggestionList(disciplinas: MutableList<Disciplina>) {
        this.disciplinas = disciplinas
        isClickable = false
        notifyDataSetChanged()
    }

    fun initCurricularMatrix(disciplinas: MutableList<Disciplina>, semestre: Int) {
        this.disciplinas = disciplinas.filter { it.disciplina.semestre == semestre }
        isClickable = true
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return disciplinas.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DisciplinaViewHolder {
        return DisciplinaViewHolder.build(parent)
    }

    override fun onBindViewHolder(holder: DisciplinaViewHolder, position: Int) {
        holder.bind(disciplinas[position],position,  isClickable, onCLick)
    }

}