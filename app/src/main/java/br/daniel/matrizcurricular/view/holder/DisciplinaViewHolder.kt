package br.daniel.matrizcurricular.view.holder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.daniel.matrizcurricular.R
import br.daniel.matrizcurricular.model.Disciplina
import kotlinx.android.synthetic.main.item_disciplina.view.*

class DisciplinaViewHolder(val view: View): RecyclerView.ViewHolder(view) {

    companion object {
        fun build(viewGroup: ViewGroup): DisciplinaViewHolder {
            val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_disciplina, viewGroup, false)
            return DisciplinaViewHolder(view)
        }
    }

    fun bind(disciplina: Disciplina, position: Int, isClickable: Boolean, onClick: (position: Int) -> Unit) {
        with(view) {
            if (isClickable) {
                setOnClickListener {
                    disciplina.isComplete = !disciplina.isComplete
                    onClick(position)
                }
                completa.visibility = if (disciplina.isComplete) View.VISIBLE else View.GONE
            }

            codigoDisciplina.text = disciplina.disciplina.codigo
            nomeDisciplina.text = disciplina.disciplina.nome
        }
    }

}