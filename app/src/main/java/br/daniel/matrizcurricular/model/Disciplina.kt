package br.daniel.matrizcurricular.model

class Disciplina(val disciplina: CComputacao) : Comparable<Disciplina> {

    var preRequisito: ArrayList<Disciplina> = ArrayList()
    var requisito: ArrayList<Disciplina> = ArrayList()

    var isComplete: Boolean = false

    fun addRequisito(disciplina: Disciplina) {
        requisito.add(disciplina)
    }

    fun addPreRequisito(disciplina: Disciplina) {
        preRequisito.add(disciplina)
    }

    fun hasPreRequisito(): Boolean {
        return preRequisito.size != 0
    }

    fun hasRequisito(): Boolean {
        return requisito.size != 0
    }

    fun isPreRequisitoComplete(): Boolean {
        preRequisito.forEach { disciplina ->
            if (!disciplina.isComplete) {
                return false
            }
        }
        return true
    }

    fun isRequisitoComplete(): Boolean {
        requisito.forEach { disciplina ->
            if (!disciplina.isComplete) {
                return false
            }
        }
        return true
    }

    override fun compareTo(other: Disciplina): Int {
        return when {
            this.requisito.size > other.requisito.size -> -1
            this.requisito.size < other.requisito.size ->  1
            else -> 0
        }
    }
}